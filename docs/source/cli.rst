Using the cli
==============

After installation you will have the ``archive-drs`` command available via
the command line. Let's inspect the general help menu of the command first:

.. code:: console

    archive-drs --help

.. execute_code::
    :hide_code:
    :hide_headers:

    from subprocess import run, PIPE
    res = run(["archive-drs", "--help"], stdout=PIPE, stderr=PIPE, check=True)
    print(res.stdout.decode())

Command line options
--------------------

You will have to provide one out of two so called positional arguments. Those
are either

- ``get`` to retrieve data or,
- ``add`` to add data

from/to the storage system. The other arguments of the ``archive-drs``
main command are optional. Below you can find a brief explanation of the
available options:

``-t``, ``--threads``
~~~~~~~~~~~~~~~~~~~~~~
The archiving task will be sub-divided into different sub-tasks which run in
parallel (threads). You can specify the number of parallel tasks running
at the same by using the ``--threads`` flag.

.. note::

   If something isn't working as expected it might be a good idea to set the
   number of threads to 1 using ``--threads 1``

``-a``, ``--arch-system``
~~~~~~~~~~~~~~~~~~~~~~~~~
The ``--arch-system`` flag lets you choose the data storage or archiving system
you want to add data to or retrieve data from. Currently available are the
`StrongLink HSM` tape archive (``hsm``) and the `OpenStack Swift` cloud storage
system ``swift``.

``-p``, ``--project``
~~~~~~~~~~~~~~~~~~~~~
Use the ``--project`` to set the data project that is charged for archival.
Meaning the data will be added to this data project.

``-u``, ``--user``
~~~~~~~~~~~~~~~~~
Set the username for logging on to the archive system. If None given (default)
the current username will be taken.

``-c``, ``--container``
~~~~~~~~~~~~~~~~~~~~~~~
Namespace of the archiving system. Container names are the upper most
"folders" that are created on the archiving system. For example on the
``hsm`` system a container name of ``mydata`` would cause a creation
of the ``/arch/<project>/mydata`` "folder" object.


Adding data to the storage system
---------------------------------
The ``add`` sub-command  of ``archive-drs`` lets you add data to the storage
system. Let's inspect the ``--help`` command first:

.. code:: console

    archive-drs add --help

.. execute_code::
    :hide_code:
    :hide_headers:

    from subprocess import run, PIPE
    res = run(["archive-drs", "add", "--help"], stdout=PIPE, stderr=PIPE)
    print(res.stdout.decode())


As you can see from the above help menu, you will have to give at least `one`
argument - the input directory. This is the directory where the data is stored.

.. warning::

   It is currently not supported to archive `single` files. Only directories
   are supported.

Additional optional arguments are:

``-r``, ``--root-dir``
~~~~~~~~~~~~~~~~~~~~~
The root directory of the data. Target object paths (file names) on the archival
system will be stored relative to this directory. For example if you want to
store data from ``/home/myuser/mydata/`` and set the ``--root-dir`` to
``/home/myuser`` the data will be accessible on the archival system under
``<container_name>/mydata``. In case of the `hsm` system this would be
``/arch/<project_name>/<container_name>/mydata``

``-s``, ``--suffixes``
~~~~~~~~~~~~~~~~~~~~~~
Set the allowed file types that are added to the archive.


Retrieving data from the storage system
----------------------------------------

The counter part of ``add`` is ``get`` sub-command of ``archive-drs``.
As the name suggests it lets you download data from the storage system.
Again, let's inspect the ``--help`` command first:

.. code:: console

    archive-drs get --help

.. execute_code::
    :hide_code:
    :hide_headers:

    from subprocess import run, PIPE
    res = run(["archive-drs", "get", "--help"], stdout=PIPE, stderr=PIPE)
    print(res.stdout.decode())


As you can see you need exactly `two` arguments:

1. The path to the directory that will be downloaded from the archive system.
2. The path where the downloaded content should be stored to.


Examples
--------
Let's look at some more specific use case examples to understand the directory
structures.

Archiving data
~~~~~~~~~~~~~~

First let's assume you want to add all netCDF data that is stored
in ``/work/ab123/cordex/EUR-11/CLMcom`` to the `hsm` system under the archive
directory ``/arch/xy987/archive``. One question you should ask yourself is
how much of the original input directory should be archived on the storage
system?

In the above example you'd have `two` sensible options. You could add
the content of the folder ``cordex/EUR-11/CLMcom`` in ``/arch/xy987/archive``
or only add the ``EUR-11/CLMcom`` directory. In the former case you would have
to set the ``--root-dir`` flag to ``/work/ab123`` and in the latter case to
``/work/ab123/cordex``. Let's construct two commands for both case examples:

.. code:: console

    archive-drs -p xy987 -c archive add /work/ab123/cordex/EUR-11/CLMcom \
        -r /work/ab123 -s .nc -s .nc4


.. code:: console

    archive-drs -p xy987 -c archive add /work/ab123/cordex/EUR-11/CLMcom \
        -r /work/ab123/cordex -s .nc -s .nc4


The first command would store the folder under the following storage path on
the hsm system (hsm is the default system):
``/arch/xy987/archive/cordex/EUR-11/CLMcom`` while the second adds the data
to the following path ``/arch/xy987/archive/EUR-11/CLMcom``. Consequently the
following commands are equivalent:

.. code:: console

    archive-drs -p xy987 -c archive add /work/ab123/cordex/EUR-11/CLMcom
        -r /work/ab123 -s .nc -s .nc4

.. code:: console

    archive-drs -p xy987 -c archive/cordex add /work/ab123/cordex/EUR-11/CLMcom
        -r /work/ab123/cordex -s .nc -s .nc4

Although valid it is strongly recommended to refrain from using the last
approach, because the resulting paths on the `hsm` and the `swift` systems would
differ. On `hsm` a sub folder in ``archive`` named ``cordex`` would be
created while on `swift` a container named ``archive/cordex`` would exists.


Retrieving data
~~~~~~~~~~~~~~~
Now let's assume we want to re-download the in the above example archived data.
This can be achieved by involving the ``get`` sub-command. This time we want
to download the data to a local data location - ``/work/db456``. To do so
we construct the following ``get`` command:


.. code:: console

    archive-drs -p xy987 get /arch/xy987/archive/cordex/EUR-11/CLMcom \
        /work/db456/cordex

The above command would download all data `underneath`
``/arch/xy987/archive/cordex/EUR-11/CLMcom`` into ``/work/db456/cordex``,
meaning that we would loose the ``EUR-11/CLMcom`` sub-folders. If we wanted
to keep those sub-folders (which we probably wanted) we would simply add this
sub-folder to the ``get`` command:

.. code:: console

    archive-drs -p xy987 get /arch/xy987/archive/cordex/EUR-11/CLMcom \
        /work/db456/cordex/EUR-11/CLMcom
