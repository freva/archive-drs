The ``archive_drs`` package
===========================

This section outlines the general usage of the :py:mod:`drs_archive` python
package. For more details on the content of this library visit the
:ref:`Api Reference <api>` section.


.. automodule:: archive_drs
   :members:
   :show-inheritance:

.. _archive_drs:
