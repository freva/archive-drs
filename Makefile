# makefile used for testing
#
#
all: install test

.PHONY: docs
install:
	python3 -m pip install -e .[tests,docs]

test:
	python3 -m pytest -vv $(PWD)/src/archive_drs/tests
	rm -rf '='

test_coverage:
	python3 -m pytest -vv \
	    --cov=$(PWD)/src/archive_drs --cov-report html:coverage_report \
		--cov-report=xml --junitxml report.xml
	rm -rf '='
	python3 -m coverage report

docs:
	make -C docs clean
	make -C docs html


lint:
	mypy --install-types --non-interactive
	black --check -t py310 -l 79 src
	flake8 src/archive_drs --count --max-complexity=10 --max-line-length=88 --statistics --doctests
