"""Swift storage integration."""

from __future__ import annotations
from getpass import getuser
from pathlib import Path
from urllib.parse import urlparse

from swift_helper import Swift as SwiftHelper
from swift_helper.utils import get_client
from swiftclient import client
import xarray as xr

from .utils import ArchiveBase, logger


class Swift(ArchiveBase):
    """Integration of the OpenStack swift cloud storage.

    Parameters
    ----------


    container: str
        Name of the swift container that is used to archive the data
    username: str | None, default: None
        Swift user name to log on to the swift account
        if None given (defulat) the the current user is set.
    group: str | None, default: None
        Swift user group to log on to the swift account
        if None given (defulat) the the current user is set.
    password: str | None, default: None
        Password for the user name to log into the system. The password can
        be overridden by setting the ``LC_TELEPHONE`` environment variable.
    """

    file_suffix: str = ".zarr"
    """The file type that is stored on the object sotre."""

    def __init__(
        self,
        container: str,
        user: str | None = None,
        group: str | None = None,
        password: str | None = None,
    ) -> None:
        group = group or getuser()
        super().__init__(password, suffixes=(".nc4", ".nc"))
        self._connection = SwiftHelper(group, user, password)
        self.container = container
        self.container_url = self._create_container(container)
        self.auth_token = self._connection.auth_token
        self.storage_url = self._connection.storage_url
        self.auth = self._connection.fsspec_auth

    def _create_container(self, container: str) -> str:
        status = self._connection.create_container(container)
        if status not in (201, 202):
            raise ValueError(f"Could not created container {container}")
        self._connection.make_public(container)
        return self._connection.get_swift_container_url(container)

    @property
    def hash_key(self) -> str:
        """Property defining the key name of the data hash."""
        return "checksum"

    @property
    def prefix(self) -> str:
        """Property defining the path prefix of the archiving system."""
        return self.container_url

    def delete(self, path: str) -> None:
        """Delete on object in the swift storage."""
        arch_path = Path("/".join(urlparse(path).path.split("/")[3:]))
        self._connection.delete(self.container, arch_path)

    def get_metadata(self, arch_path: str, value_key: str) -> str:
        """Get metadata of a file archived file object.

        Parameters
        ----------
        input_path: Path
            path to object holding the metadata.
        value_key: str
            the metadata key that should be retrieved.

        Returns
        -------
        str: string representation of the metadata
        """

        try:
            return xr.open_zarr(arch_path).attrs.get(value_key, "")
        except Exception:
            return ""

    def archive(
        self, archive_path: str, input_path: Path, **kwargs: str | Path | None
    ) -> None:
        """Push data to the archive.

        Parameters
        ----------
        archive_path: Path
            Path to the file_object that should be archived.
        input_path: Path
            Posix path to data directory that should be archived.
        """
        checksum = kwargs.pop("checksum", "")
        logger.info("Archiving data: %s to %s", input_path, archive_path)
        out_file = "/".join(urlparse(archive_path).path.split("/")[3:])
        client.put_object(
            self.storage_url,
            self.auth_token,
            self.container,
            out_file,
            None,
            content_type="application/directory",
        )
        files = [
            f for f in input_path.rglob("*.*") if f.suffix in self.suffixes
        ]
        dset = xr.open_mfdataset(
            sorted(files),
            combine="by_coords",
            parallel=True,
            decode_times=False,
        )

        dset.attrs[self.hash_key] = checksum or ""
        for data_var in dset.data_vars:
            try:
                dset[data_var] = dset[data_var].chunk("auto")
            except Exception:
                pass
        dset.to_zarr(
            archive_path,
            mode="a",
            storage_options={"get_client": get_client, "auth": self.auth},
        )  # type: ignore

    def retrieve(self, arch_path: str, target_path: Path) -> None:
        """Retrieve data from the archiving system.

        Parameters
        ----------
        arch_path: str
            Path to the file object that is retrieved from the archive.
        target_path: Path
            Path to the destitation where the retrieved output should go to.
        """
        path = Path(arch_path.removeprefix(self.container_url))

        target_path = target_path.expanduser().absolute()
        for inp_dir, _ in self._connection.list(
            self.container, path, dir_only=True
        ):
            if inp_dir.suffix == ".zarr":
                logger.debug("Retrieving %s", inp_dir.name)
                url = f"{self.container_url}/{inp_dir}"
                out_path = (target_path / inp_dir).with_suffix(".nc")
                out_path.parent.mkdir(exist_ok=True, parents=True)
                with xr.open_zarr(
                    url,
                    storage_options={
                        "get_client": get_client,
                        "auth": self.auth,
                    },
                    decode_times=False,
                ) as dset:
                    dset.to_netcdf(out_path, engine="h5netcdf")
