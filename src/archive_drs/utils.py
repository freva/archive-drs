"""Utility functions."""


from __future__ import annotations
from abc import abstractmethod
import base64
import json
import logging
import os
from pathlib import Path
import shlex
from subprocess import run, SubprocessError, PIPE
from typing import Any, Callable, Iterator
import threading

from cftime import date2num
import xarray as xr

logging.basicConfig(format="%(name)s - %(levelname)s - %(message)s")
logger: logging.Logger = logging.getLogger("archive")
logger.setLevel(logging.INFO)


class ArchiveBase:
    """Base class for interacting with any archiving system."""

    file_suffix: str = ""
    _archive_root: str = ""

    def __init__(
        self,
        password: str | None = None,
        suffixes: tuple[str, ...] = (
            ".tar",
            ".nc",
            ".zarr",
            ".grb",
            ".gz",
            "tgz",
        ),
    ) -> None:

        self.env = os.environ.copy()
        self._password = password
        self.suffixes = suffixes
        self._threads: list[threading.Thread] = []

    @property
    def password(self) -> str:
        """Evaluate a given password or read an env variable as password."""

        _env_passwd = os.environ.get("LC_TELEPHONE", "")
        if _env_passwd:
            _env_passwd = base64.b64decode(_env_passwd.encode()).decode()
        return self._password or _env_passwd

    def _list_dir(self, input_dir: str | Path) -> list[str]:
        """This method has to be implemented by the child class."""
        return []

    @abstractmethod
    def archive(
        self, archive_path: str, input_path: Path, **kwargs: str | Path | None
    ) -> None:
        """Abstract method for pushing data to the archive.

        Parameters
        ----------
        archive_path: str
            Path to the file_object that should be archived.
        input_path: Path
            Posix path to data that should be archived.
        """

    @abstractmethod
    def delete(self, path: str) -> None:
        """Abstract method for deleting objects in the archive

        Parameters
        ----------
        path: str
            Path to the to file object that should be deleted in the archive.
        """

    @abstractmethod
    def get_metadata(self, input_path: str, value_key: str) -> str:
        """Abstract method for getting metadata of a file archived file object.

        Parameters
        ----------
        input_path: str
            path to storage object holding the metadata
        value_key: str
            the metadata key that should be retrieved.

        Returns
        -------
        str: string representation of the metadata
        """

    def is_alive(self) -> bool:
        """Check if threads are still running.

        Returns
        -------
        bool: if any thread setting metadata is still alive.
        """
        return any([t.is_alive() for t in self._threads])

    @abstractmethod
    def retrieve(self, arch_path: str, target_path: Path) -> None:
        """Abstract method to retrieve data from the archiving system.

        Parameters
        ----------
        arch_path: str
            Path to the file object that is retrieved from the archive.
        target_path: Path
            Path to the destination where the retrieved output should go to.
        """

    def run_command(
        self, command: str | list[str], strip: bool = True
    ) -> list[str]:
        """Run a archive command and return the stdout in a list of strings.

        Parameters
        ----------
        command: str | list[str]
            Command that is going to be executed.

        strip: bool, default: True
            strip leading and tailing white spaces from each stdout line.

        Returns
        -------
        list[str]:
            stdout of the command, each line an entry in the list.

        Raises
        ------
        ValueError:
            If command was not successful
        """

        if isinstance(command, str):
            cmd_list = shlex.split(command)
        else:
            cmd_list = command
        try:
            res = run(
                cmd_list, env=self.env, check=True, stdout=PIPE, stderr=PIPE
            )
        except SubprocessError as error:
            raise ValueError(f"{command} was not successful") from error
        if strip:
            return [
                o.strip() for o in res.stdout.decode().split("\n") if o.strip()
            ]
        return res.stdout.decode().split("\n")

    @property
    @abstractmethod
    def hash_key(self) -> str:
        """Abstract property defining the key entry holding the data."""

    @property
    @abstractmethod
    def prefix(self) -> str:
        """Abstract property defining the path prefix of the archiving system."""

    def walk_dir(self, input_dir: Path) -> Iterator[Path]:
        """Recursively get all file objects in a given 'directory' object.

        Parameters
        ----------
        input_dir: str | Path
            Directory that shall be listed.

        Yields
        ------
        Path:
            File like 'object'.
        """

        try:
            content = self._list_dir(str(input_dir))
        except (ValueError, IndexError):
            content = []
        for sub_dir in content:
            if (input_dir / sub_dir).suffix in self.suffixes:
                yield input_dir / sub_dir
            else:
                yield from self.walk_dir(input_dir / sub_dir)


def background(
    func: Callable[..., Any]
) -> Callable[..., threading.Thread | None]:
    """Threading decorator

    use @background above the function you want to run in the background
    """

    def backgrund_func(*args: Any, **kwargs: Any) -> threading.Thread | None:
        # Test coverage doesn't work very well in this multi threadded env
        # the serial switch is mainly for unit testing purpose to run
        # the decorated function serial.
        serial = kwargs.pop("_serial", False)
        if serial:
            func(*args, **kwargs)
            return None
        thread = threading.Thread(target=func, args=args, kwargs=kwargs)
        thread.start()
        return thread

    return backgrund_func


def get_paths(
    input_dir: Path, suffixes: list[str] | None = None
) -> Iterator[Path]:

    suffixes = suffixes or [".nc4", ".grb", ".nc", ".grib", ".grb2"]
    for file in input_dir.rglob("*.*"):
        if file.suffix in suffixes:
            yield file


def metadatadata_from_directory(
    input_dir: Path, suffixes: list[str] | None = None
) -> str:
    """Gather metadata in a directory."""
    attrs: dict[str, Any] = {}
    files = sorted(map(str, get_paths(input_dir, suffixes)))
    print(files)
    with xr.open_mfdataset(
        files, combine="by_coords", parallel=True, use_cftime=True
    ) as dset:
        attrs["global"] = {str(k): str(v) for k, v in dset.attrs.items()}
        attrs["dims"] = []
        attrs["data_vars"] = []
        for _name, dim in dset.dims.items():
            name = str(_name)
            attrs[name] = {
                str(k).lower(): v for (k, v) in dset[name].attrs.items()
            }
            attrs["dims"].append(name)
            attrs[name]["size"] = str(dim)
            if name != "time":
                attrs[name]["start"] = str(dset[name].values[0])
                attrs[name]["end"] = str(dset[name].values[-1])
            else:
                units = "seconds since 1970-01-01T00:00:00"
                start, end = dset[name].values[0], dset[name].values[-1]
                attrs[name]["units"] = units
                attrs[name]["calendar"] = start.calendar
                attrs[name]["start"] = str(
                    date2num(start, units, start.calendar)
                )
                attrs[name]["end"] = str(date2num(end, units, end.calendar))
        for data_var in map(str, dset.data_vars):
            attrs["data_vars"].append(data_var)
            attrs[data_var] = {
                str(k): str(v) for (k, v) in dset[data_var].attrs.items()
            }
            attrs[data_var]["dims"] = dset[data_var].dims

    return json.dumps(attrs)
