"""Command line Interface of the archiver."""


from __future__ import annotations
import argparse
from concurrent import futures
from getpass import getuser
import logging
from pathlib import Path
import sys
import time
from typing import Union

from .utils import logger
from .archive import Archive, ARCHIVE_SYSTEMS


class Cli:
    """Command line interface definition.

    Properties
    ----------
        kwargs: dict[str, [str | float| Path]]
        property holding all parsed keyword arguments.
    """

    kwargs: dict[str, Union[str, float, Path, int]] = {}
    verbose: int = 0
    desc: str = "Archive freva data paths"

    @staticmethod
    def add(args: argparse.Namespace, _arch: Archive | None = None) -> None:
        """Arichve directories."""
        with futures.ThreadPoolExecutor(args.threads) as pool:
            arch = _arch or Archive(
                args.input_dir,
                args.project,
                tuple(args.suffixes),
                archive_system=args.arch_system,
                root_dir=args.root_dir,
                user=args.user,
                container=args.container,
            )
            tasks, itt = [], 0
            for (directory, obj_file) in arch:
                tasks.append(
                    pool.submit(arch.archive_directory, directory, obj_file)
                )
                itt += 1
                if itt == args.threads:
                    _ = [t.result() for t in futures.as_completed(tasks)]
                    itt, tasks = 0, []
            _ = [t.result() for t in futures.as_completed(tasks)]
            while arch.is_alive():
                time.sleep(1)

    @staticmethod
    def get(args: argparse.Namespace, _arch: Archive | None) -> None:
        """Retrieve directories from archive."""
        arch = _arch or Archive(
            (args.input_dir,),
            args.project,
            archive_system=args.arch_system,
            container=args.container,
            file_suffix=args.file_suffix,
        )
        arch.retrieve_directory(args.input_dir, args.root_dir)

    def __init__(self, parser: argparse.ArgumentParser | None = None):
        """Instantiate the CLI class."""
        self.parser = parser or argparse.ArgumentParser(
            prog="archive-drs",
            description=self.desc,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        self.parser.add_argument(
            "-v",
            action="count",
            help="Verbosity level",
            default=0,
        )
        self.parser.add_argument(
            "-t",
            "--threads",
            help="Number of threads",
            type=int,
            default=6,
        )
        self.subparsers = self.parser.add_subparsers(
            help="Available sub-commands:",
            required=True,
        )
        self.parser.add_argument(
            "-a",
            "--arch-system",
            help="The archival system that should be used.",
            type=str,
            default="hsm",
            choices=ARCHIVE_SYSTEMS,
        )
        self.parser.add_argument(
            "-p",
            "--project",
            help="The name of the data project",
            default="bb1203",
            type=str,
        )
        self.parser.add_argument(
            "-u",
            "--user",
            help=(
                "Username used to log on the to storage system. If none given "
                "(default) the current user is taken"
            ),
            type=str,
            default=getuser(),
        )
        self.parser.add_argument(
            "-c",
            "--container",
            help=(
                "Container name used to add/retrieve data to/frome the sotrage "
                "system. "
            ),
            default="arch",
            type=str,
        )
        self._verbose = 0
        self._add_archive_cmd()
        self._add_get_cmd()

    def _add_archive_cmd(self) -> None:
        """Add sub commands for archiving data."""

        parser = self.subparsers.add_parser(
            "add",
            help="Add cmorised data to the archive",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        parser.add_argument(
            "input_dir",
            metavar="input_dir",
            type=Path,
            help="The input directory for the data.",
            nargs="+",
        )
        parser.add_argument(
            "-r",
            "--root-dir",
            type=Path,
            help=(
                "The root directory of the data: "
                "if None given (default) the root dir will be set to "
                "/work/<project_name>"
            ),
            default=None,
        )
        parser.add_argument(
            "-f",
            "--file-type",
            type=str,
            help=(
                "The file type of the files that are archived,"
                " only applicable for tape archival type (hsm)"
            ),
            default=".tar",
        )
        parser.add_argument(
            "-s",
            "--suffixes",
            type=str,
            default=[".nc4", ".nc", ".grb", ".grib"],
            nargs="+",
            action="append",
            help="Set the file name suffixes that are archived",
        )
        parser.set_defaults(apply_func=self.add)

    def _add_get_cmd(self) -> None:
        """Add sub commands for retrieving data from the archive."""

        parser = self.subparsers.add_parser(
            "get",
            help="Retrieve cmorised data from the archive",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        parser.add_argument(
            "input_dir",
            metavar="input_dir",
            type=Path,
            help="The data input directory on the object store.",
        )
        parser.add_argument(
            "root_dir",
            metavar="root_dir",
            type=Path,
            help="The root directory the data should be saved to.",
        )
        parser.set_defaults(apply_func=self.get)

    @property
    def log_level(self) -> int:
        """Get the log level from verbosity."""
        return max(logging.ERROR - 10 * (self._verbose + 1), logging.DEBUG)

    def parse_args(self, argv: list[str]) -> argparse.Namespace:
        """Parse the arguments.
        Parameters
        ----------
        argv: list[str]
            List of command line arguments that is parsed.

        Returns
        -------
        argparse.Namespace
            parsed Namespace object.
        """

        arguments = self.parser.parse_args(argv or None)
        self._verbose = arguments.v
        logger.setLevel(self.log_level)
        return arguments


def cli(argv: list[str] | None = None, _arch: Archive | None = None) -> None:
    """Method the creates the command line argument parser.

    Parameters
    ----------
    argv: list[str]
        command line arguments that are going to be passed.
    _arch: Archive | None, default: None
        pre-defined instance of the archive object, this is only used for
        unit testing.
    """
    arg_p = Cli()
    argv = argv or sys.argv[1:]
    arguments = arg_p.parse_args(argv or sys.argv[1:])
    arguments.apply_func(arguments, _arch)
