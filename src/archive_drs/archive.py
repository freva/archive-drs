"""Module to archive files to in an archive system."""

from __future__ import annotations
from getpass import getuser
import hashlib
import os
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import cast, Generator

from .utils import logger, ArchiveBase
from ._hsm import HSM
from ._swift import Swift

SCRATCH: str = "/scratch"
WORK_DIR: str = "/work"
ARCHIVE_SYSTEMS: tuple[str, str] = ("hsm", "swift")


class Archive:
    """Interface to interact with the archival system.

    Parameters
    ----------
    input_paths: str, pathlib.Path, list[str, pathlib.Path]
        Input directory or files that should be pushed/retrieved to/from the
        archive system.
    project: str
        The name of the data project the archived data will be added to.
        *Note*: This project will be "charged" for data archival.
    user: str, default: None
        Set the username for logging on to the archive system. If None given
        (default) the current username will be taken.
    suffixes: list[str], default: None
        Filename suffixes that should be used.
    archive_system: str, default: hsm
        The name of the archiving system. Currently available are the
        `StrongLink HSM` tape archive (``hsm``) and the `OpenStack Swfit` cloud
        storage system, ``swift``.
    root_dir: str, pathlib.Path, default: None
        Directory path to the data that should be archived. Target object paths
        (file names) on the archival are stored relative to this directory.
        For example if you want to store data from ``/home/myuser/mydata/`` and
        set the ``root-dir`` keyword to ``/home/myuser`` then data will be
        accessible on the archival system under ``<container_name>/mydata``.
        The same goes for retrieving data. If you wanted to get data from
        ``/arch/<project>/<container>/mydata`` and set the ``root_dir`` keyword
        to ``/home/myuser`` then the data is stored in ``/home/myuser/mydata``.
    container: str, default: arch
        Namespace of the archiving system. Container names are the upper most
        "folders" that are created on the archiving system. For example on the
        ``hsm`` system a container name of ``mydata`` would cause a creation
        of the ``/arch/<project>/mydata`` "folder" object.

    Raises
    ------
    ValueError:
        If the ``archive_system`` keyword doesn't match one of the supported
        archival systems.

    Attributes
    ----------
    input_paths: str | pathlib.Path | tuple[str | pathlib.Path, ...]
        The given input paths.
    suffixes: tuple[str]
        The file name suffixes of potential target files that are archived.
    directories: list[tuple[Path, Path]]
        Get all sub directories for the input directory.
    file_type: str
        The file suffix of the object store.
    scratch: pathlib.Path
        Get the scratch directory of the current user.

    Methods
    -------
    archive_directory:
        Sent the content of a directory to the archive system.
    convert_str_to_timestamp:
        Convert a string representation of a time step to an ISO timestamp.
    is_alive:
        Check if threads are still running.
    retrieve_directory:
        Reverse method of ``archive_directory``, gets directory from
        archive.

    Examples
    --------
    Let's assume you want to interact with the hsm archiving system to
    add or retrieve data. To do so you will have to create an instance
    of the :py:class:`Archive` class. It is important to note that you will
    always have to pass the ``input_dir`` argument, no matter whether you want
    to add or get data. Only the meaning of ``input_dir`` changes. For example,
    if you want to add data ``input_dir`` would mean a directory residing on
    the file system that is about to be added to the archival system.
    In case you wanted to retrieve data, ``input_dir`` would mean the path
    to the object store holding data that should be downloaded.

    For example suppose you wanted to add all netCDF data that is
    stored in ``/work/ab123/cordex/EUR-11/CLMcom`` to the `hsm` system under
    the archive "directory" ``/work/ab123/cordex/EUR-11/CLMcom``. One question
    you should ask yourself is how much of the original input directory should
    be archived on the storage system?

    In the above example you'd have `two` sensible options. You could add
    the content of the folder ``cordex/EUR-11/CLMcom`` in ``/arch/xy987/archive``
    or only add the ``EUR-11/CLMcom`` directory. In the former case you would
    have to set the ``root_dir`` argument to ``/work/ab123`` and the latter case
    to ``/work/ab123/cordex``. Let's construct two instances of the archive
    class for the two cases:


    ::

        from archive_drs import Archive
        hsm_arch1 = Archive("/work/ab123/cordex/EUR-11/CLMcom",
                            "xy987",
                            container="archive",
                            root_dir="/work/ab123",
                            suffixes=(".nc", ".nc4"),
                            archive_system="hsm")
        hsm_arch2 = Archive("/work/ab123/cordex/EUR-11/CLMcom",
                            "xy987",
                            container="archive"
                            root_dir="/work/abc123/cordex",
                            archive_system="hsm")
                            )

    Instance ``hsm_arch1`` creates folders relative to ``/work/ab123`` while
    ``hsm_arch2`` would work with directories that are relative to
    ``/work/ab123/cordex``.
    """

    def __init__(
        self,
        input_paths: str | Path | tuple[str | Path, ...],
        project: str,
        suffixes: tuple[str, ...] | None = None,
        archive_system: str = "hsm",
        root_dir: str | Path | None = None,
        _session_path: str | Path | None = None,
        **kwargs: str | None,
    ) -> None:
        self.suffixes = suffixes or (".nc4", ".nc", ".grb", ".grib")
        self.input_paths = input_paths
        self._data_dir = Path(root_dir or (Path(WORK_DIR) / project))
        self._scratch = Path(SCRATCH)
        self._paths: list[tuple[Path, Path]] | None = None
        container = kwargs.pop("container", "arch") or "arch"
        suffix = kwargs.pop("file_type", None) or ".tar"
        if archive_system == "hsm":
            self._arch = cast(
                ArchiveBase,
                HSM(
                    self.suffixes + (".tar",),
                    session_path=_session_path,
                    user=kwargs.get("user"),
                    group=project,
                    container=container,
                    file_type=suffix,
                ),
            )
        elif archive_system == "swift":
            self._arch = cast(
                ArchiveBase,
                Swift(container, group=project, **kwargs),
            )  # pragma: no cover
        else:
            raise ValueError(
                f"archive system should be one of {', '.join(ARCHIVE_SYSTEMS)}"
            )

    @property
    def directories(self) -> list[tuple[Path, Path]]:
        """Get all sub directories for the input directory."""
        if self._paths is None:
            self._paths = list(iter(self))
        return self._paths

    def retrieve_directory(
        self, input_directory: Path | str, root_directory: Path | str
    ) -> None:
        """Reverse method of :py:func:`archive_directory`, gets directory from archive.

        Parameters
        ----------
        input_directory: Path
            The path of that should be retrieved.
        root_directory: Path
            Target path where the retrieved output is saved to.
            directories


        Examples
        ---------
        The reverse method of :py:func:`archive_directory` is the
        :py:func:`retrieve_directory` method. The only difference is that
        in case you want to retrieve data you would have to create an instance
        of the :py:class:`Archive` class using the path to the object store
        on the archive system as input:

        ::

            from archive_drs import Archive
            hsm_arch = Archive("/arch/xy987/archive/cordex/",
                               "xy987",
                               container="archive",
                               archive_system="hsm",
                               root_dir="/arch/xy987/archive/cordex")
            hsm_arch.retrieve_directory("/arch/xy987/archive/cordex/EUR-11/CLMcom",
                                        "~/my_cordex")


        See Also
        --------
        :py:func:`archive_directory`: Add data to the archival system
        """
        self._arch.retrieve(
            str(input_directory), Path(root_directory).expanduser().absolute()
        )

    @property
    def file_suffix(self) -> str:
        """The file suffix of the object store."""
        return self._arch.file_suffix

    def archive_directory(
        self, directory: Path | str, destfile: Path | str
    ) -> None:
        """Sent the content of a directory to the archive system.

        The archival is skipped if a path on the archive system exists and
        hasn't been updated since it was archived.

        Parameters
        ----------
        directory: Path
            The path of the directory that should be archived.
        destfile: Path
            The location in the archival system.


        Examples
        --------
        Let's assume we want create a `hsm` archive of the directory content
        in ``/work/ab123/cordex/EUR-11/CLMcom`` to ``/arch/xy987/archive``.
        In this case the data project that is "charged" with the archived
        data is ``xy987`` and its container name would be ``archive``. The
        data is stored in the same directory structure that is relative to
        ``/work/ab123`` on the archive system.

        ::

            from archive_drs import Archive
            hsm_arch = Archive("/work/ab123/cordex/EUR-11/CLMcom",
                               "xy987",
                               container="archive",
                               root_dir="/work/ab123")
            for inp_dir, arch_file in hsm_arch:
                hsm_arch.archive_directory(inp_dir, arch_file)


        In the same manner you could involve the swift object store:

        ::

            from archive_drs import Archive
            swift_arch = Archive("/work/ab123/cordex/EUR-11/CLMcom",
                                 "xy987",
                                 container="archive",
                                 root_dir="/work/ab123",
                                 archive_system="swift")
            for inp_dir, arch_file in swift_arch:
                swift_arch.archive_directory(inp_dir, arch_file)

        See Also
        --------
        :py:func:`retrieve_directory`: Get data from the archival system

        """
        directory, destfile = Path(directory), Path(destfile)
        new_md5 = self._get_hash(directory)
        rel_path = destfile.relative_to(self._data_dir)
        arch_path = f"{self._arch.prefix}/{rel_path}"
        logger.debug("Retrieving information for %s", arch_path)
        old_md5 = self._arch.get_metadata(arch_path, self._arch.hash_key)
        if new_md5 != old_md5:
            if old_md5:
                self._arch.delete(os.path.dirname(arch_path))
            self._arch.archive(
                arch_path,
                directory,
                checksum=new_md5,
                temp_dir=self.scratch,
            )
        else:
            logger.debug("Skipping %s", directory)

    def is_alive(self) -> bool:
        """Check if threads are still running.

        Returns
        -------
        bool: if any thread setting metadata is still alive.
        """
        return self._arch.is_alive()

    def _iter_dir(self, directory: Path) -> Generator[Path, None, None]:

        for inp in directory.rglob("*.*"):
            if inp.suffix in self.suffixes:
                yield inp

    def _get_hash(self, directory: Path) -> str:
        """Calculate the hash of a directory."""
        with NamedTemporaryFile() as temp_file:
            with open(temp_file.name, "w", encoding="utf-8") as f_obj:
                for inp in self._iter_dir(directory):
                    f_obj.write(f"{inp.name}: {inp.stat().st_mtime}\n")
            return hashlib.md5(open(temp_file.name, "rb").read()).hexdigest()

    @staticmethod
    def convert_str_to_timestamp(time_str: str, alternative: str = "0") -> str:
        """Convert a string representation of a time step to an iso timestamp

        Parameters
        ----------
        time_str: str
            Representation of the time step usually of form %Y%m%dT%H%M or
            a variant such as %Y%m or %Y%m%d
        alternative: str, default: 0
            If conversion fails the alternative/default value the time step
            get's assign to

        Returns
        -------
        str:
            ISO time string representation of the input time step, such as
             %Y %Y-%m-%d or %Y-%m-%dT%H%M%S
        """
        if not time_str.strip():
            return alternative
        time_str = "".join(filter(str.isdigit, time_str))
        # Not valid if time repr empty or starts with a letter, such as 'fx'
        l_times = len(time_str)
        if not l_times:
            return alternative
        if l_times <= 4:
            # Suppose this is a year only
            return time_str.zfill(4)
        if l_times <= 6:
            # Suppose this is %Y%m or %Y%e
            return f"{time_str[:4]}{time_str[4:].zfill(2)}"
        if l_times <= 8:
            # Suppose this is %Y%m%d
            return f"{time_str[:4]}{time_str[4:6]}{time_str[6:].zfill(2)}"
        date = f"{time_str[:4]}{time_str[4:6]}{time_str[6:8]}"
        time = time_str[8:]
        if len(time) <= 2:
            time = time.zfill(2)
        else:
            # Always drop seconds
            l_time = min(4, len(time))
            time = time[:2] + time[2:l_time].zfill(2)
        return f"{date}T{time}"

    def _directory_to_file(self, dirname: Path) -> list[Path]:
        """Create a tar filename of a directory."""
        obj_files: list[Path] = []
        files: dict[str, list[str]] = {}
        for file_name in dirname.rglob("*.*"):
            if file_name.suffix in self.suffixes:
                time = file_name.with_suffix("").name.split("_")[-1]
                prefix = "_".join(file_name.name.split("_")[:-1])
                start, _, end = time.partition("-")
                start_str = self.convert_str_to_timestamp(start, "0")
                end_str = self.convert_str_to_timestamp(end, "9999")
                if prefix not in files:
                    files[prefix] = [start_str]
                else:
                    files[prefix].append(start_str)
                files[prefix].append(end_str)
        for fname, times in files.items():
            new_file = f"{fname}_{min(times)}-{max(times)}{self.file_suffix}"
            obj_files.append(dirname / new_file)
        return obj_files

    def __iter__(self) -> Generator[tuple[Path, Path], None, None]:
        """Iterate over all found data files."""
        if isinstance(self.input_paths, (list, tuple, set)):
            input_paths = tuple(self.input_paths)
        else:
            input_paths = (self.input_paths,)
        output_dirs: list[Path] = []
        for input_path in map(Path, input_paths):
            if input_path.is_file() and input_path.suffix in self.suffixes:
                output_dirs.append(input_path.parent)
            elif input_path.is_dir():
                for input_file in input_path.rglob("*.*"):
                    if input_file.suffix in self.suffixes:
                        output_dirs.append(input_file.parent)
            else:
                # This is a long shot, we assume that a glob pattern was given
                for input_file in input_path.parent.rglob(input_path.name):
                    output_dirs.append(input_file.parent)
        self._paths = []
        for output_dir in sorted(set(output_dirs)):
            for tar_file in self._directory_to_file(output_dir):
                self._paths.append((output_dir, tar_file))
                yield output_dir, tar_file

    @property
    def scratch(self) -> Path:
        """Get the scratch directory of the current user."""
        scratch_dir = self._scratch / getuser()[0] / getuser()
        return scratch_dir
