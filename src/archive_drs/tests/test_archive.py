"""Tests of the archive module."""

from pathlib import Path
import pytest
from tempfile import TemporaryDirectory
import time

import xarray as xr


def test_iter_dirs(patch_file: Path, netcdf_files: Path) -> None:
    """Test the iteration of directories."""

    from archive_drs import Archive

    inp_file = tuple(netcdf_files.rglob("*.*"))
    arch = Archive(netcdf_files, "the_project", _session_path=patch_file)
    for inp_, tar_file in arch:
        assert inp_ in [inp.parent for inp in inp_file]
        assert tar_file.suffix == ".tar"
        assert "precip" in tar_file.with_suffix("").name
    arch = Archive(inp_file, "the_project", _session_path=patch_file)
    for inp_, tar_file in arch:
        assert inp_ in [inp.parent for inp in inp_file]
        assert tar_file.suffix == ".tar"
        assert "precip" in tar_file.with_suffix("").name
    arch = Archive(
        netcdf_files / "*.*", "the_project", _session_path=patch_file
    )
    assert len(arch.directories) == 1
    for inp_, tar_file in arch:
        assert inp_ in [inp.parent for inp in inp_file]
        assert tar_file.suffix == ".tar"
        assert "precip" in tar_file.with_suffix("").name
    assert len(arch.directories) == 1


def test_convert_str_timestamp() -> None:
    """Test the convert_str_to_timestamp method."""

    from archive_drs import Archive

    assert (
        Archive.convert_str_to_timestamp("2020-01-01T00:01") == "20200101T0001"
    )
    assert Archive.convert_str_to_timestamp("2020-01-01T01") == "20200101T01"
    assert Archive.convert_str_to_timestamp("2020-01-01") == "20200101"
    assert Archive.convert_str_to_timestamp("2020-01") == "202001"
    assert Archive.convert_str_to_timestamp("2020-fx") == "2020"
    assert Archive.convert_str_to_timestamp("fx", "0") == "0"
    assert Archive.convert_str_to_timestamp("", "0") == "0"


def test_right_archival_system(netcdf_files: Path) -> None:
    """Test the functionality of the right archival system."""

    from archive_drs import Archive
    from archive_drs._hsm import HSM

    arch1 = Archive(netcdf_files, "the_project")
    assert isinstance(arch1._arch, HSM)
    with pytest.raises(ValueError):
        Archive(netcdf_files, "the_project", archive_system="foo")


def test_base_methods() -> None:

    from archive_drs.utils import ArchiveBase

    assert ArchiveBase()._list_dir(Path("foo")) == []  # type: ignore


def test_archive_and_retrieve_directory(
    patch_file: Path, netcdf_files: Path, archive_dir: Path
) -> None:
    """Test directory retrieval."""
    from archive_drs import Archive

    with TemporaryDirectory() as temp_dir:
        for path in ("scratch", "work"):
            (Path(temp_dir) / path).mkdir(exist_ok=True, parents=True)
        arch = Archive(netcdf_files, "the_project", _session_path=patch_file)
        arch._data_dir = netcdf_files
        arch._scratch = Path(temp_dir) / "scratch"
        arch._scratch.mkdir(exist_ok=True, parents=True)
        for input_dir, tar_file in arch:
            arch.archive_directory(input_dir, tar_file)
            result = list(archive_dir.rglob("*.tar"))
            assert len(result) >= 1
            m_time = result[0].stat().st_mtime
            time.sleep(0.5)
            arch.archive_directory(input_dir, tar_file)
            result = list(archive_dir.rglob("*.tar"))
            assert m_time == result[0].stat().st_mtime
            inp_file = [str(f) for f in input_dir.rglob("*.nc")][0]
            with xr.open_dataset(inp_file) as dset:
                dset = dset.load()
            dset.to_netcdf(inp_file)
            arch.archive_directory(input_dir, tar_file)
            result = list(archive_dir.rglob("*.tar"))
            assert m_time != result[0].stat().st_mtime
            arch.retrieve_directory(result[0].parent, Path(temp_dir) / "work")
            assert len(list((Path(temp_dir) / "work").rglob("*.tar"))) >= 1
            assert isinstance(arch.is_alive(), bool)
