"""pytest definitions to run the unittests."""
from __future__ import annotations
import base64
from functools import partial
import json
import os
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Any, Generator
import shutil
from subprocess import CalledProcessError

import pytest
import mock
import numpy as np
import pandas as pd
import xarray as xr


class SubProcess:
    def __init__(self, stdout: list[str]) -> None:

        self._stdout = "\n".join(stdout)

    @property
    def stdout(self) -> bytes:
        return self._stdout.encode()


class RequestMock:
    def __init__(
        self,
        url: str,
        data: dict[str, dict[str, str]] | None = None,
        headers: dict[str, str] | None = None,
        out: dict[str, dict[str, str]] | None = None,
        **kwargs: Any,
    ) -> None:

        self.url = url
        self.data = data
        self.headers = headers
        self.out = out

    def json(self) -> dict[str, dict[str, str]]:
        """Mock the json get value."""
        return self.out or {}

    @classmethod
    def post(cls, url: str, **kwargs: Any) -> RequestMock:
        """Mock the rest post method."""
        return cls(url, **kwargs)

    @classmethod
    def get(cls, url: str, **kwargs: Any) -> RequestMock:
        """Mock the rest get method."""
        return cls(url, **kwargs)


# flake8: noqa: C901
def run(command: list[str], **kwargs: Any) -> SubProcess:
    """Patch the subprocess.run command."""

    main_command, sub_cmd = command[0:2]
    if main_command.startswith("slk"):
        if sub_cmd == "archive":
            arch_dir = Path(command[3])
            inp_file = Path(command[2])
            arch_dir.mkdir(exist_ok=True, parents=True)
            if inp_file.is_file():
                shutil.copy(f"{inp_file}", f"{arch_dir}/{inp_file.name}")
            else:
                shutil.copytree(f"{inp_file}", f"{arch_dir}/{inp_file.name}")
            cmd = ["cp", f"{inp_file}", f"{arch_dir}/"]
        elif sub_cmd == "delete":
            shutil.rmtree(command[-1])
            cmd = ["rm", "-r", command[-1]]
        elif sub_cmd == "tag":
            inp_file = Path(command[3]).with_suffix(".json")
            inp_file.parent.mkdir(exist_ok=True, parents=True)
            tags: dict[str, dict[str, str]] = {}
            for tag in command[4:]:
                key, _, value = tag.partition("=")
                main_key, _, sub_key = key.partition(".")
                try:
                    tags[main_key][sub_key] = value
                except KeyError:
                    tags[main_key] = {sub_key: value}
            try:
                with inp_file.open("w") as f_obj:
                    json.dump(tags, f_obj)
            except FileNotFoundError:
                raise CalledProcessError(1, ["tag"])
            cmd = ["echo", "success"]
        elif sub_cmd == "list":
            try:
                cmd = [str(p) for p in Path(command[-1]).iterdir()]
            except NotADirectoryError:
                raise CalledProcessError(1, ["list"])
        elif sub_cmd == "retrieve":
            arch_path = Path(command[-2])
            target = Path(command[-1])
            target.parent.mkdir(exist_ok=True, parents=True)
            if arch_path.is_file():
                shutil.copy(f"{arch_path}", f"{target}")
            else:
                if target.is_dir():
                    shutil.rmtree(target)
                shutil.copytree(f"{arch_path}", f"{target}/")
            cmd = [""]
        elif sub_cmd == "metadata":
            inp_file = Path(command[2]).parent.with_suffix(".json")
            if not inp_file.is_file():
                raise CalledProcessError(1, ["metadata"])
            else:
                with inp_file.open() as f_obj:
                    attrs = json.load(f_obj)
                string = []
                for main_key, values in attrs.items():
                    string.append(f"{main_key}")
                    for key, attr in values.items():
                        string.append(f"   {key}:  {attr}")
                cmd = string

    return SubProcess(cmd)


def create_data(variable_name: str, size: int) -> xr.Dataset:
    """Create a netcdf dataset."""
    coords: dict[str, np.ndarray] = {}
    coords["x"] = np.linspace(-10, -5, size)
    coords["y"] = np.linspace(120, 125, size)
    lat, lon = np.meshgrid(coords["y"], coords["x"])
    lon_vec = xr.DataArray(lon, name="Lg", coords=coords, dims=("y", "x"))
    lat_vec = xr.DataArray(lat, name="Lt", coords=coords, dims=("y", "x"))
    coords["time"] = np.array(
        [
            np.datetime64("2020-01-01T00:00"),
            np.datetime64("2020-01-01T12:00"),
            np.datetime64("2020-01-02T00:00"),
            np.datetime64("2020-01-02T12:00"),
        ]
    )
    dims = (4, size, size)
    data_array = np.empty(dims)
    for time in range(dims[0]):
        data_array[time] = np.zeros((size, size))
    dset = xr.DataArray(
        data_array,
        dims=("time", "y", "x"),
        coords=coords,
        name=variable_name,
    )
    data_array = np.zeros(dims)
    return xr.Dataset(
        {variable_name: dset, "Lt": lon_vec, "Lg": lat_vec}
    ).set_coords(list(coords.keys()))


@pytest.fixture(scope="session")
def patch_file(session_path: Path) -> Generator[Path, None, None]:
    req = {"data": {"attributes": {"session_key": "secret"}}}
    post = partial(RequestMock.post, out=req)
    env = os.environ.copy()
    env["LC_TELEPHONE"] = base64.b64encode("foo".encode()).decode()
    with mock.patch.dict(os.environ, env, clear=True):
        with mock.patch("requests.post", post):
            with mock.patch("requests.get", RequestMock.get):
                with mock.patch("subprocess.run", run):
                    yield session_path


@pytest.fixture(scope="session")
def archive_dir(netcdf_files: Path) -> Generator[Path, None, None]:
    from archive_drs._hsm import HSM

    root = HSM._archive_root
    with TemporaryDirectory() as temp_dir:
        HSM._archive_root = temp_dir
        yield Path(temp_dir)
    HSM._archive_root = root


@pytest.fixture(scope="session")
def save_dir() -> Generator[Path, None, None]:
    """Crate a temporary directory."""
    with TemporaryDirectory() as td:
        yield Path(td)


@pytest.fixture(scope="session")
def data() -> Generator[xr.Dataset, None, None]:
    """Define a simple dataset with a blob in the middle."""
    dset = create_data("precip", 100)
    yield dset


@pytest.fixture(scope="session")
def netcdf_files(
    data: xr.Dataset,
) -> Generator[Path, None, None]:
    """Save data with a blob to file."""

    with TemporaryDirectory() as td:
        for time in (data.time[:2], data.time[2:]):
            time1 = pd.Timestamp(time.values[0]).strftime("%Y%m%d%H%M")
            time2 = pd.Timestamp(time.values[1]).strftime("%Y%m%d%H%M")
            out_file = (
                Path(td)
                / "the_project"
                / "test1"
                / "precip"
                / f"precip_{time1}-{time2}.nc"
            )
            out_file.parent.mkdir(exist_ok=True, parents=True)
            data.sel(time=time).to_netcdf(out_file)
        yield Path(td)


@pytest.fixture(scope="session")
def session_path() -> Generator[Path, None, None]:
    with TemporaryDirectory() as temp_dir:
        yield Path(temp_dir) / "slk.json"
