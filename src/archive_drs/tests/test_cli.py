"""Tests for the command line interface."""
import logging
from pathlib import Path
from tempfile import TemporaryDirectory

import pytest


def test_help(patch_file: Path, archive_dir: Path) -> None:
    """Test the help stirng."""
    from archive_drs.cli import cli

    with pytest.raises(SystemExit):
        cli(["--help"])
    with pytest.raises(SystemExit):
        cli(["add"])
    with pytest.raises(SystemExit):
        cli(["get"])


def test_loglevel(patch_file: Path, archive_dir: Path) -> None:
    """Check the default log level."""
    from archive_drs.cli import Cli

    arg_p = Cli()
    _ = arg_p.parse_args(["add", "foobar"])
    assert arg_p.log_level == logging.WARNING
    _ = arg_p.parse_args(["-v", "add", "foobar"])
    assert arg_p.log_level == logging.INFO
    _ = arg_p.parse_args(["-vv", "add", "foobar"])
    assert arg_p.log_level == logging.DEBUG


def test_add(patch_file: Path, netcdf_files: Path, archive_dir: Path) -> None:
    """Test the archiving cli."""
    from archive_drs import Archive
    from archive_drs.cli import cli

    with TemporaryDirectory() as temp_dir:
        for path in ("scratch", "work"):
            (Path(temp_dir) / path).mkdir()
        arch = Archive(netcdf_files, "the_project", _session_path=patch_file)
        arch._data_dir = netcdf_files
        arch._scratch = Path(temp_dir) / "scratch"
        cli(
            [
                "--threads",
                "1",
                "--project",
                "the_project",
                "add",
                f"{netcdf_files}",
            ],
            _arch=arch,
        )
        result = list(archive_dir.rglob("*.tar"))
        assert len(result) == 1


def test_get(patch_file: Path, netcdf_files: Path, archive_dir: Path) -> None:
    """Test the archiving cli."""
    from archive_drs import Archive
    from archive_drs.cli import cli

    with TemporaryDirectory() as temp_dir:
        for path in ("scratch", "work"):
            (Path(temp_dir) / path).mkdir()
        arch = Archive(netcdf_files, "the_project", _session_path=patch_file)
        arch._data_dir = netcdf_files
        arch._scratch = Path(temp_dir) / "scratch"
        root = arch._arch._archive_root
        arch._arch._archive_root = str(netcdf_files)
        cli(
            [
                "--project",
                "the_project",
                "add",
                f"{netcdf_files}",
            ],
            _arch=arch,
        )
        cli(
            [
                "get",
                f"{netcdf_files}",
                f"{arch._scratch}",
            ],
            _arch=arch,
        )
        result = list(arch._scratch.rglob("*.tar"))
        assert len(result) == 1
        arch._arch._archive_root = root
