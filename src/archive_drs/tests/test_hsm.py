import json
from pathlib import Path
from tempfile import TemporaryDirectory
import time


def test_login(patch_file: Path) -> None:

    from archive_drs._hsm import HSM

    _ = HSM((".tar",), password="foo", session_path=patch_file)
    assert patch_file.is_file()
    with patch_file.open() as f_obj:
        assert "sessionKey" in f_obj.read()


def test_archive(
    patch_file: Path, netcdf_files: Path, archive_dir: Path
) -> None:

    from archive_drs._hsm import HSM

    hsm = HSM((".tar",), password="foo", session_path=patch_file)
    inp_file = list(netcdf_files.rglob("*.nc"))[0]
    out_file = archive_dir / "foo" / "bar" / inp_file.name
    hsm.archive(str(out_file), inp_file)
    assert out_file.exists()


def test_delete(
    patch_file: Path, netcdf_files: Path, archive_dir: Path
) -> None:

    from archive_drs._hsm import HSM

    hsm = HSM((".tar",), password="foo", session_path=patch_file)
    inp_file = list(netcdf_files.rglob("*.nc"))[0]
    out_file = archive_dir / "foo" / "bar" / inp_file.name
    hsm.archive(str(out_file), inp_file)
    hsm.delete(str(out_file.parent))
    assert out_file.parent.exists() is False


def test_list(patch_file: Path, netcdf_files: Path, archive_dir: Path) -> None:

    from archive_drs._hsm import HSM

    hsm = HSM((".nc",), password="foo", session_path=patch_file)
    inp_file = list(netcdf_files.rglob("*.nc"))[0]
    out_file = archive_dir / "foo" / "bar" / inp_file.name
    hsm.archive(str(out_file), inp_file)
    out = list(hsm.walk_dir(archive_dir))
    assert out_file in out
    assert len(out) == 1
    out = list(hsm.walk_dir(out_file))
    assert out == []


def test_retrieve(
    patch_file: Path, netcdf_files: Path, archive_dir: Path
) -> None:

    from archive_drs._hsm import HSM

    hsm = HSM((".nc",), password="foo", session_path=patch_file)
    inp_file = list(netcdf_files.rglob("*.nc"))[0]
    out_file = archive_dir / "foo" / "bar" / inp_file.name
    hsm.archive(str(out_file), inp_file)
    with TemporaryDirectory() as temp_dir:
        hsm.retrieve(str(out_file), Path(temp_dir))
        assert (Path(temp_dir) / out_file.name).exists()


def test_set_metadata(
    patch_file: Path, netcdf_files: Path, archive_dir: Path
) -> None:

    from archive_drs._hsm import HSM

    hsm = HSM(
        (".nc",), password="foo", session_path=patch_file, file_type=".nc"
    )
    inp_file = list(netcdf_files.rglob("*.nc"))[0]
    out_file = archive_dir / "foo" / "bar" / inp_file.name
    print(inp_file)
    hsm.archive(str(out_file), inp_file)
    hsm._set_metadata(out_file, inp_file.parent, "foobar")
    assert hsm.is_alive()
    time.sleep(0.1)
    json_file = out_file.parent.with_suffix(".json")
    assert json_file.exists()
    with json_file.open() as f_obj:
        tags = json.load(f_obj)
    json_file.unlink()
    assert "document" in tags
    assert "Version" in tags["document"]
    assert tags["document"]["Version"] == "foobar"
    assert "Keywords" in tags["document"]
    assert "precip" in tags["document"]["Keywords"]
    hsm._set_metadata_thread(
        out_file, inp_file.parent / "foo", "foo-bar", _serial=True
    )
    json_file = out_file.parent.with_suffix(".json")
    with json_file.open() as f_obj:
        tags = json.load(f_obj)
    assert tags["document"]["Version"] == "foo-bar"
    assert tags["document"]["Keywords"] == "{}"


def test_get_metadata(
    patch_file: Path, netcdf_files: Path, archive_dir: Path
) -> None:

    from archive_drs._hsm import HSM

    hsm = HSM((".nc",), password="foo", session_path=patch_file)
    inp_file = list(netcdf_files.rglob("*.nc"))[0]
    out_file = archive_dir / "foo" / "bar" / inp_file.name
    hsm.archive(str(out_file), inp_file)
    assert hsm.get_metadata(str(out_file / "bar"), "document.Version") == ""
    hsm._set_metadata_thread(out_file, inp_file.parent, "foobar", _serial=True)
    assert hsm.get_metadata(str(out_file), "document.Version") == "foobar"
    assert hsm.get_metadata(str(out_file), "foo.bar") == ""
