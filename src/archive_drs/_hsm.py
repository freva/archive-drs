"""Integrations for the StrongLink Archiving System."""

from __future__ import annotations
from datetime import datetime, timedelta
from getpass import getpass, getuser
import json
from pathlib import Path
import tarfile
from tempfile import TemporaryDirectory
import threading
import warnings
from typing import cast

import requests
import yaml
import xarray as xr
from .utils import ArchiveBase, background, logger, metadatadata_from_directory


def _get_expiration_date(session_path: Path) -> datetime:
    """Get the expiration date of the session key."""
    fmt = "%a %b %d %H:%M:%S %Z %Y"
    now = datetime.now().astimezone().strftime(fmt)
    try:
        with session_path.open() as f_obj:
            date = json.load(f_obj).get("expireDate", now)
    except FileNotFoundError:
        date = now
    return datetime.strptime(date, fmt)


class HSM(ArchiveBase):
    """Integration of the StrongLink system via the HSM class

    Parameters
    ----------

    suffixes: tuple[str]
        Allowed file suffixes of file types that are added to the arichive.
    username: str | None, default: None
        User name used for logging into the system, if ``None`` given (default)
        the current user will be taken.
    password: str | None, default: None
        Password for the user name to log into the system. The password can
        be overridden by setting the ``LC_TELEPHONE`` environment variable.
    group: str | None, default: None
        The data group that is used for achiving the data
    file_type: str, default: .tar
        the type of file that is pushed to the archive. If .tar is given
        (default) the tar archive is created first.
    """

    url: str = "https://archive.dkrz.de/api/v2/authentication"
    _archive_root: str = "/arch"

    def __init__(
        self,
        suffixes: tuple[str, ...],
        user: str | None = None,
        password: str | None = None,
        group: str | None = None,
        file_type: str = ".tar",
        session_path: str | Path | None = None,
        container: str = "arch",
    ) -> None:

        _session_path = (
            session_path or Path("~").expanduser() / ".slk" / "config.json"
        )
        super().__init__(password, suffixes)
        self.container = container
        self.session_path = Path(_session_path)
        self.group = group or getuser()
        self.file_suffix = file_type
        self.password_prompt = "Password to logon to the hsm system:"
        self.username = user or getuser()
        self.env["PATH"] += ":/sw/spack-levante/slk-3.3.21-5xnsgp/bin"
        self.env["PATH"] += ":/sw/spack-levante/openjdk-17.0.0_35-k5o6dr/bin"
        self.env["JAVA_HOME"] = "/sw/spack-levante/openjdk-17.0.0_35-k5o6dr"
        self.login()

    def _login_via_request(self, passwd: str) -> None:

        self.session_path.parent.mkdir(exist_ok=True, parents=True)
        data = {
            "data": {
                "attributes": {
                    "domain": "ldap",
                    "name": self.username,
                    "password": passwd,
                },
                "type": "authentication",
            }
        }
        headers = {"Content-type": "application/json"}
        fmt = "%a %b %d %H:%M:%S %Z %Y"
        exp_date = (
            (datetime.now() + timedelta(days=1)).astimezone().strftime(fmt)
        )
        with warnings.catch_warnings():
            warnings.simplefilter(action="ignore")
            res = requests.post(
                self.url, data=json.dumps(data), headers=headers, verify=False
            )
        key = (
            res.json()
            .get("data", {})
            .get("attributes", {})
            .get("session_key", "")
        )
        if key:
            sec = {
                "user": getuser(),
                "sessionKey": key,
                "expireDate": exp_date,
            }
            with self.session_path.open("w", encoding="utf-8") as f_obj:
                json.dump(sec, f_obj)
            self.session_path.chmod(0o600)

    def login(self) -> None:
        """Login to the hsm system."""

        now = datetime.now()
        exp_date = _get_expiration_date(self.session_path)
        diff = (exp_date - now).total_seconds()
        if diff <= 0:
            passwd = self.password or getpass(self.password_prompt)
            self._login_via_request(passwd)

    def _list_dir(self, input_dir: str | Path) -> list[str]:

        _dirs = self.run_command(f"slk list {input_dir}")
        return [d.strip().split()[-1] for d in _dirs if d.split()[-1].strip()]

    def _write(self, temp_file: Path | str, directory: Path) -> None:
        temp_file = Path(temp_file)
        if directory.is_file():
            inp_files = [directory]
        else:
            inp_files = sorted(
                inp
                for inp in directory.rglob("*.*")
                if inp.suffix in self.suffixes
            )
        if self.file_suffix == ".tar":
            with tarfile.open(temp_file, "w") as tar_obj:
                for inp in inp_files:
                    logger.debug(
                        "Adding %s to tarfile %s",
                        inp.name,
                        Path(temp_file).name,
                    )
                    tar_obj.add(str(inp))
        else:
            with xr.open_mfdataset(
                sorted(inp_files), combine="by_coords"
            ) as dset:
                logger.debug(
                    "Creating netcdf file %s",
                    Path(temp_file).name,
                )
                dset.to_netcdf(temp_file)

    def archive(
        self, archive_path: str, input_path: Path, **kwargs: str | Path | None
    ) -> None:
        """Push data to the archive.

        Parameters
        ----------
        archive_path: str
            Path to the file_object that should be archived.
        input_dir: Path
            Posix path to data directory that should be archived.
        checksum: str, default: None
            Check of the of data to archive, if None given (default) no
            checksum is added.
        temp_dir: Path, default: None
            Directory of that stores temporary files.
        """
        arch_path = Path(archive_path).expanduser().absolute()
        checksum = cast(str, kwargs.get("checksum", ""))
        temp_dir = kwargs.get("temp_dir")
        logger.info(
            "Archiving data: %s to %s", input_path.name, arch_path.parent
        )
        if temp_dir:
            Path(temp_dir).mkdir(exist_ok=True, parents=True)
        with TemporaryDirectory(dir=temp_dir) as write_dir:
            temp_file = Path(write_dir) / arch_path.name
            self._write(temp_file, input_path)
            self.run_command(f"slk archive {temp_file} {arch_path.parent}")
        self._set_metadata(arch_path, input_path, checksum)

    def delete(self, path: str) -> None:
        """Delte file object in the archive

        Parameters
        ----------
        path: str
            Path to the to file object that should be deleted in the archive.
        """

        logger.info("Deleting old directory %s", path)
        self.run_command(f"slk delete -R {path}")

    def get_metadata(self, input_path: str, value_key: str) -> str:
        """Get metadata of a file archived file object.

        Parameters
        ----------
        input_path: str
            path to object holding the metadata.
        value_key: str
            the metadata key that should be retrieved.

        Returns
        -------
        str:
            string representation of the metadata of the object store
        """
        key, _, sub_key = value_key.partition(".")
        try:
            meta_data = self.run_command(
                f"slk_helpers metadata {input_path}", strip=False
            )
        except ValueError:
            logger.debug("Could not read metadata %s", Path(input_path).name)
            return ""
        lines = []
        for line in meta_data:
            if line and line[0] == line[0].strip():
                lines.append(f"{line}:")
            elif line:
                lines.append(line)
        try:
            metadata = yaml.safe_load("\n".join(lines)) or {}
        except Exception:  # pragma: no cover
            logger.warning(
                "Could not read metadata %s", Path(input_path).name
            )  # pragma: no cover
            return ""  # pragma: no cover
        return metadata.get(key, {}).get(sub_key, "")

    @property
    def hash_key(self) -> str:
        """Property defining the key name of the data hash."""
        return "document.Version"

    @property
    def prefix(self) -> str:
        """Property defining the path prefix of the archiving system."""
        return str(Path(self._archive_root) / self.group / self.container)

    def retrieve(self, arch_path: str, target_path: Path) -> None:
        """Retrieve data from the archiving system.

        Parameters
        ----------
        arch_path: str
            Path to the file object that is retrieved from the archive.
        target_path: Path
            Path to the destitation where the retrieved output should go to.
        """
        if Path(arch_path).is_absolute():
            arch_p = Path(arch_path)
        else:
            arch_p = Path(self.prefix) / arch_path.removeprefix(self.container)
        logger.debug("Retrieving %s", Path(arch_p).name)
        self.run_command(f"slk retrieve -R {arch_p} {target_path}")

    @background
    def _set_metadata_thread(
        self,
        archive_path: Path,
        input_dir: Path,
        md5sum: str,
        **kwargs: bool,
    ) -> None:
        logger.debug("Setting metadata for %s", archive_path)
        try:
            metadata = metadatadata_from_directory(input_dir)
        except Exception as error:
            logger.warning(
                "Could not gather metadata for %s\n%s", input_dir, error
            )
            metadata = "{}"
        keys = [f"document.Version={md5sum}", f"document.Keywords={metadata}"]
        cmd = ["slk", "tag", "-R", f"{archive_path.parent}"] + keys
        self.run_command(cmd)

    def _set_metadata(
        self, archive_path: Path, input_dir: Path, md5sum: str
    ) -> None:
        """Set metadata of an archived path.

        Currently a string representation of the netcdf dataset and a md5sum
        of the archived data is set as metadata.

        Parameters
        ----------
        archive_path: Path
            Path to the file object that is archived
        input_dir: Path
            Posix path to the directory holding the data
        md5sum: str
            Md5sum of the archive_path
        """
        thread = self._set_metadata_thread(archive_path, input_dir, md5sum)
        if isinstance(thread, threading.Thread):
            self._threads.append(thread)
