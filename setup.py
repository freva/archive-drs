from pathlib import Path
import os
from setuptools import setup, find_packages
import sys
from tempfile import TemporaryDirectory


def read(*parts):
    script_path = os.path.dirname(os.path.realpath(sys.argv[0]))
    return open(os.path.join(script_path, *parts)).read()


def find_version(*parts):

    old_path = sys.path.copy()
    __version__ = ""
    with TemporaryDirectory() as td:
        with open(os.path.join(td, "tmp_version.py"), "w") as f_obj:
            for line in read(*parts).split("\n"):
                if "__version__" in line:
                    f_obj.write(line)
        sys.path.insert(0, td)
        try:
            from tmp_version import __version__
        finally:
            sys.path = old_path
        if __version__:
            return __version__
        raise RuntimeError("Unable to find version string.")


def get_stubs(*parts):
    asset_dir = Path(__file__).parent.joinpath(*parts)
    out = [str(d.relative_to(asset_dir.parent)) for d in asset_dir.rglob("*.pyi")]
    return out


setup(
    name="archive-drs",
    packages=find_packages("src"),
    description="Add DRS Files to archive.",
    author="CLINT",
    author_email="freva@dkrz.de",
    long_description=read("README.md"),
    include_package_data=True,
    long_description_content_type="text/markdown",
    license="BSD-3-Clause",
    python_requires=">=3.7",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: BSD License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Topic :: Scientific/Engineering :: Physics",
        "Topic :: Scientific/Engineering :: Atmospheric Science",
    ],
    version=find_version("src", "archive_drs", "__init__.py"),
    package_data={"": get_stubs("src", "archive_drs")},
    package_dir={"": "src"},
    project_urls={
        "Documentation": "https://freva.gitlab-pages.dkrz.de/archive-drs/",
        "Issues": "https://gitlab.dkrz.de/freva/archive-drs/-/issues",
        "Source": "https://gitlab.dkrz.de/freva/archive-drs",
    },
    entry_points={
        "console_scripts": [
            "archive-drs = archive_drs.cli:cli",
            "freva-archive-drs = archive_drs.cli:cli",
        ]
    },
    install_requires=[
        "cftime",
        "xarray",
        "dask",
        "pyyaml",
        "h5netcdf",
        "netCDF4",
        "swift_helper@git+https://gitlab.dkrz.de/freva/swift-helper.git",
    ],
    extras_require={
        "tests": [
            "black",
            "pytest",
            "pandas",
            "mock",
            "numpy",
            "requests-mock",
            "pytest-env",
            "pytest-cov",
            "testpath",
            "flake8",
            "mypy",
            "types-mock",
            "types-PyYAML",
            "types-requests",
            "types-setuptools",
        ],
        "docs": [
            "sphinx",
            "sphinx-rtd-theme",
            "sphinxcontrib_github_alt",
            "numpydoc",
            "sphinx-execute-code-python3",
        ],
    },
)
