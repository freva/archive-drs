# Welcome to archive-drs's documentation!

Archive data following the Data Reference Syntax.

The software aims to archive data that follows directory structures according
to the Data Reference Syntax (DRS). This is case for data that has been added
to the `freva` data stack.

Currently the code supports the following archiving systems:

- [StongLink HSM](https://docs.dkrz.de/doc/datastorage/hsm/index.html)

The intended usage is via a command line interface (cli) that let's you archive
and retrieve archived data. Along with the data additional meta data is stored.
This metadata consists of a hash that represents the data files that are
archived and the modification dates of the data. The idea is that data
that has already been archived and that hasn't changed since it was archived
will not be archived again. This allows for automated archival, like in cron
jobs.

Furthermore a string representation of the dataset is stored in the metadata
allowing for metadata inspection of the data without downloading the data.


## Use Case

The code expects the data that is supposed to be archived to be organised in
a file name structure following the DRS conventions. The operates on
``directory`` basis not on ``file`` bases. Meaning that all files in a given
directory will be packed to a ``.tar`` file an archived.


## Installation

The code can be installed using ``pip``:

```console
   python3 -m pip install archive-drs --extra-index-url gitlab.dkrz.de/api/v4/projects/139393/packages/pypi/simple
```

## Usage

Visit the [documentation](https://freva.gitlab-pages.dkrz.de/archive-drs)
for more details on the usage of this software.
